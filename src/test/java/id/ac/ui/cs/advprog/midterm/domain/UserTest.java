package id.ac.ui.cs.advprog.midterm.domain;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;


public class UserTest {
    @Autowired
    private static User user = new User();

    @Test
    void checkIfNameEqualsAndExists() {
        user.setName("zahra");
        assertEquals("zahra", user.getName());
        assertNotNull(user.getName());
    }

    @Test
    void checkIfEmailEqualsAndExists() {
        user.setEmail("zahra@email.com");
        assertEquals("zahra@email.com", user.getEmail());
        assertNotNull(user.getEmail());
    }

    @Test
    void checkIfIdEqualsAndExists() {
        user.setId(123);
        assertEquals(123, user.getId());
    }

    @Test
    void checkToStringMethod() {
        user.setId(123);
        user.setName("zahra");
        user.setEmail("zahra@email.com");
        assertEquals("User{id=123, name='zahra', email='zahra@email.com'}", user.toString());
    }

}
