package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.domain.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @MockBean
    private UserRepository userRepository;

    private User user = new User();


    @BeforeEach
    void setUpUser() {
        user = new User();
        user.setId(123);
        user.setName("zahra");
        user.setEmail("zahra@email.com");
        when(userRepository.findById(user.getId()))
                .thenReturn(java.util.Optional.ofNullable(user));
    }

    @Test
    void checkIfShowSignUpTemplate() throws Exception{
        mockMvc.perform(get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    void checkIfAddUserValidThenShowTemplateIfNotValidTheStay() throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void checkIfAddUserNotValidThenStay() throws Exception {
        user.setEmail(null);
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
//                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    void checkIfValidThenShowShowUpdateFormTemplate() throws Exception{
        mockMvc.perform(get("/edit/" + user.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"))
                .andExpect(model().attributeExists("user"));
    }

    @Test
    void checkIfShowUpdateFormTemplateButIdNotFoundThenThrowException() {
        try {
            mockMvc.perform(get("/edit/0"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("update-user"))
                    .andExpect(model().attributeExists("user"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:0", e.getMessage());

//            assertEquals("Request processing failed; nested exception is java.lang.NullPointerException", e.getMessage());
        }
    }

    @Test
    void checkIfUpdateUserValidThenShowTemplate() throws Exception {
        mockMvc.perform(post("/update/" + user.getId())
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void checkIfUpdateUserNotValidThenStay() throws Exception {
        user.setEmail(null);
        mockMvc.perform(post("/update/" +  user.getId())
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    void checkIfValidThenShowDeleteUserTemplate() throws Exception{
        mockMvc.perform(get("/delete/" + user.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void checkIfShowDeleteUserTemplateButIdNotFoundThenThrowException() {
        try {
            mockMvc.perform(get("/delete/0"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:0", e.getMessage());
//            assertEquals("Request processing failed; nested exception is java.lang.NullPointerException", e.getMessage());
        }
    }
}
